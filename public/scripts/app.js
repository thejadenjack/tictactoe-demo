$(document).ready(function() {
  var socket = io();

  $('#fullpage').fullpage();

  socket.emit('start');

  socket.on('updateBoard', function(tiles) {
    console.log('Updating board.');

    for (var i = 0; i < 3; i += 1) {
      for (var j = 0; j < 3; j += 1) {
        $('#box' + i + '' + j).text(tiles[i][j]);
      }
    }
  });

  socket.on('reportWinner', function(winner) {
    console.log('Winner detected');

    $('#winner').text('The winner is: ' + winner + '!');
  });

  $('#restart').click(function() {
    $('#winner').text('No winner yet.');
    socket.emit('restart');
  });

  function updateHistory() {
    socket.emit('getHistory', function(games) {
      for (var i = 0; i < games.length; i += 1) {
        let last = games[i].history.pop();
        $('#game' + i).html(
          '<p align="center">Game ' + i + '</p>' +
          '<p align="center">Winner: ' + games[i].winner + '</p>' +
          '<div class="small-board">' +
          '  <div class="row">' +
          '    <button>' + last[0][0] + '</button>' +
          '    <button>' + last[1][0] + '</button>' +
          '    <button>' + last[2][0] + '</button>' +
          '  </div>' +
          '  <div class="row">' +
          '    <button>' + last[0][1] + '</button>' +
          '    <button>' + last[1][1] + '</button>' +
          '    <button>' + last[2][1] + '</button>' +
          '  </div>' +
          '  <div class="row">' +
          '    <button>' + last[0][2] + '</button>' +
          '    <button>' + last[1][2] + '</button>' +
          '    <button>' + last[2][2] + '</button>' +
          '  </div>' +
          '</div>'
        );
      }
    });
  }

  updateHistory();

  $('#refresh').click(function() {
    updateHistory();
  });

  for (var i = 0; i < 3; i += 1) {
    for (var j = 0; j < 3; j += 1) {
      (function() {
        var x = i;
        var y = j;
        $('#box' + x + '' + y).click(function(handler) {
        console.log('Box clicked.');

        socket.emit('takeTurn', { x, y });
      })})();
    }
  }
});
