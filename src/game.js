class Game {
  constructor(mode) {
    this.restart();
  }

  restart() {
    this.tiles = [['', '', ''], ['', '', ''], ['', '', '']];
    this.history = [JSON.parse(JSON.stringify(this.tiles))];
    this.turn = 'X';
    this.running = true;
    this.winner = '';
    this.huPlayer = 'X';
    this.aiPlayer = 'O';
  }

  flipTurn() {
    if (this.turn === 'X') {
      this.turn = 'O';
    } else {
      this.turn = 'X';
    }
  }

  checkWinner(board, turn) {
    // Check columns
    for (let i = 0; i < 3; i += 1) {
      for (let j = 0; j < 3; j += 1) {
        if (board[i][j] !== turn) {
          break;
        }
        if (j === 2) {
          return true;
        }
      }
    }

    // Check rows
    for (let i = 0; i < 3; i += 1) {
      for (let j = 0; j < 3; j += 1) {
        if (board[j][i] !== turn) {
          break;
        }
        if (j === 2) {
          return true;
        }
      }
    }

    // Check diagonal
    for (let d = 0; d < 3; d += 1) {
      if (board[d][d] !== turn) {
        break;
      }
      if (d === 2) {
        return true;
      }
    }

    // Check anti diagonal
    for (let ad = 0; ad < 3; ad += 1) {
      if (board[ad][2 - ad] !== turn) {
        break;
      }
      if (ad === 2) {
        return true;
      }
    }

    return false;
  }

  isCat() {
    let cat = true;
    for (let i = 0; i < 3; i += 1) {
      if (cat === true) {
        for (let j = 0; j < 3; j += 1) {
          if (this.tiles[j][i] === '') {
            cat = false;
            break;
          }
        }
      } else {
        break;
      }
    }
    return cat;
  }

  processTurn(x, y) {
    this.tiles[x][y] = this.turn;

    this.history.push(JSON.parse(JSON.stringify(this.tiles)));

    if (this.checkWinner(this.tiles, this.turn)) {
      this.winner = this.turn;
      this.running = false;
    } else {
      this.flipTurn();
    }
  }
}

module.exports = Game;
