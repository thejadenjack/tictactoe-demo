const express = require('express');
const http = require('http');
const path = require('path');
const socketio = require('socket.io');
const Game = require('./game');
const AI = require('./ai');
const Store = require('./store');

const app = express();
const server = http.Server(app);
const io = socketio(server);
const store = new Store();

app.use(express.static(path.resolve(__dirname, '../public')));

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../public/index.html'));
});

io.on('connection', (socket) => {
  const game = new Game();

  socket.on('setMode', (mode) => {
    if (mode === 'singleplayer' && mode === 'multiplayer') {
      game.mode = mode;
    }
  });

  socket.on('start', () => {
    game.running = true;
  });

  socket.on('restart', () => {
    game.restart();
    socket.emit('updateBoard', game.tiles);
  });

  socket.on('getHistory', (cb) => {
    cb(store.games);
  });

  socket.on('takeTurn', ({ x = 0, y = 0 }) => {
    if (game.running && (x >= 0 && x <= 2) && (y >= 0 && y <= 2)) {
      if (game.tiles[x][y] === '' && game.turn === 'X') {
        game.processTurn(x, y);

        socket.emit('updateBoard', game.tiles);

        const cat = game.isCat();
        if (game.winner !== '' && !cat) {
          socket.emit('reportWinner', game.winner);
          store.push({
            winner: game.winner,
            history: game.history,
          });
        } else if (!cat) {
          const ai = new AI(game);

          const move = ai.determineMovement();
          game.processTurn(move.position.x, move.position.y);

          if (game.winner !== '') {
            socket.emit('reportWinner', game.winner);
            store.push({
              winner: game.winner,
              history: game.history,
            });
          }

          socket.emit('updateBoard', game.tiles);
        } else {
          socket.emit('reportWinner', 'Cat game');
          store.push({
            winner: 'Cat-game',
            history: game.history,
          });
        }
      }
    }
  });
});

let port = 3000;

if (process.env.NODE_ENV === 'production') {
  port = 80;
}

server.listen(port , () => {
    console.log('Listening on *:' + port);
});
