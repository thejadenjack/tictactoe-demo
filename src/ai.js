class AI {
  constructor(game) {
    this.game = game;
    this.currentBoard = JSON.parse(JSON.stringify(game.tiles));
  }

  determineMovement() {
    return this.minimax(this.currentBoard, this.game.aiPlayer);
  }

  // Returns the position to place tile
  minimax(newBoard, turn) {
    let board = JSON.parse(JSON.stringify(newBoard));

    const empty = [];
    const moves = [];

    for (let i = 0; i < 3; i += 1) {
      for (let j = 0; j < 3; j += 1) {
        if (board[i][j] === '') {
          empty.push({ x: i, y: j });
        }
      }
    }

    if (this.game.checkWinner(board, this.game.huPlayer)) {
        return -10;
    } else if (this.game.checkWinner(board, this.game.aiPlayer)) {
      return 10;
    } else if (empty.length === 0) {
      return 0;
    }

    for (let i = 0; i < empty.length; i += 1) {
      let move = {
        position: { x: empty[i].x, y: empty[i].y },
      };

      board[empty[i].x][empty[i].y] = turn;

      if (turn === this.game.aiPlayer) {
        move.score = this.minimax(board, this.game.huPlayer);
      } else {
        move.score = this.minimax(board, this.game.aiPlayer);
      }

      moves.push(move);
    }

    let bestMove = null;
    if (turn === this.game.aiPlayer) {
      let bestScore = -1000;
      for (let i = 0; i < moves.length; i += 1) {
        if (moves[i].score > bestScore) {
          bestScore = moves[i].score;
          bestMove = moves[i];
        }
      }
    } else {
      let bestScore = 1000;
      for (let i = 0; i < moves.length; i += 1) {
        if (moves[i].score < bestScore) {
          bestScore = moves[i].score;
          bestMove = moves[i];
        }
      }
    }

    return bestMove;
  }
}

module.exports = AI;
