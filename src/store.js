class Store {
  constructor() {
    this.games = [];
  }

  push(game) {
    this.games.push(game);

    if (this.games.length > 5) {
      this.games.shift();
    }
  }
}

module.exports = Store;
