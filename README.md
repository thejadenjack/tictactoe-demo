## Tic-Tac-Toe Demo

Container hosted [here](https://hub.docker.com/r/jadengiordano/tictactoe).

To run Tic-Tac-Toe navigate to http://tictactoe.forester.io or just click [here]([http://tictactoe.forester.io](http://tictactoe.forester.io).

You should see this page

![home screen](https://i.gyazo.com/583ccc81e1af1820f8b2e329661db757.png)



Start by clicking on on of the boxes to place your turn. The AI will automatically choose its position and you will go after the ai again.

![X went and O went](https://i.gyazo.com/1c3e6d8c9b36fd491c22a4f20a2dd9da.png)



Continue playing until a winner has been announced.

![O is winner](https://i.gyazo.com/657935b4b5486452ee4cd4c05e24cf89.png)



Click restart to clear the board.



If you'd like to see previous games scroll down, and click refresh to get the most recent games played. Note this list is global, meaning other players games will be shown here aswell, so you may not see your game if other games have finished and replaced your score.

![Last game](https://i.gyazo.com/9c399d7f6da01d7fc0e521b24f75308c.png)

That's it, for tic tac toe.



### Technical notes

This application is stateless and uses websockets via socket.io to communicate between the client and server.

It is also containerized and hosted on the docker hub, ready to be used by any machine running docker or able to utilize docker containers.

The algorithm used for the AI is a modified form of minimax which only searches for its best move, not looking at the players moves, this helps make the game more winnable as minimax makes the AI almost impossible to beat.
